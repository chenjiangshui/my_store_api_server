const mongoose = require('mongoose');
const baseModel = require('./BaseModel');
const Schema = mongoose.Schema;

//字段介绍
/**
 * proid:商品id
 * imgName:图片名称,
 * imgUrl:图片地址
 * info:图片简介
 */

const carouselSchema = new mongoose.Schema({
    ...baseModel,
    proid: {
        type: Schema.Types.ObjectId,
        ref: 'product',
        default: null
    },
    imgName: {
        type: String,
        required: true,
    },
    imgUrl: {
        type: String,
        required: true,
    },
    info: {
        type: String,
        default: null
    }
});

module.exports = carouselSchema;