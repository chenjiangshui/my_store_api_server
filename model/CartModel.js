const mongoose = require('mongoose');
const baseModel = require('./BaseModel');
const Schema = mongoose.Schema;

//字段介绍
/**
 * userid:用户编号
 * proid:产品编号,
 * quantity:产品数量
 * name：商品名称
 * mainImageUrl:商品图片
 * status:商品状态 0未结算，1为已完成
 */

const cartSchema = new mongoose.Schema({
    ...baseModel,
    userid: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null
    },
    proid: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        default: null
    },
    quantity: {
        type: Number,
        required: true,
        default: 1
    },
    name: {
        type: String,
        required: true,
    },
    mainImageUrl: {
        type: String,
        required: true,
    },
    status: {
        type: Number,
        required: true,
        default: 0
    },
    price:{
        type: Number,
        required: true
    },
});

module.exports = cartSchema;