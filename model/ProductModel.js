const mongoose = require('mongoose');
const baseModel = require('./BaseModel');
const Schema = mongoose.Schema;

//字段介绍
/**
 * cateid:分类id
 * name:商品名称,
 * mainImageUrl:主图片地址
 * imgUrl1-5：商品图片1
 * price:价格
 * status：状态
 */

const productSchema = new mongoose.Schema({
    ...baseModel,
    cateid: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        default: null
    },
    name: {
        type: String,
        required: true,
    },
    mainImageUrl:{
        type: String,
        required: true,
    },
    imgUrl1: {
        type: String,
        required: true,
    },
    imgUrl2: {
        type: String,
        required: true,
    },
    imgUrl3: {
        type: String,
        required: true,
    },
    imgUrl4: {
        type: String,
        required: true,
    },
    imgUrl5: {
        type: String,
        required: true,
    },
    price:{
        type: Number,
        required: true
    },
    status: {
        type: Number,
        required:true,
        default: 1
    }
});

module.exports = productSchema;