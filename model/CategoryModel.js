const mongoose = require('mongoose');
const baseModel = require('./BaseModel');
const Schema = mongoose.Schema;

//字段介绍
/**
 * parentid:父类别编号，为空表示根节点
 * category_name:分类名称,
 * isChild:是否有子菜单
 * status:类别状态，1可用，0不可用
 */

const categorySchema = new mongoose.Schema({
    ...baseModel,
    parentid: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        default: null
    },
    category_name: {
        type: String,
        required: true,
    },
    isChild: {
        type: Boolean,
        required:true,
        default: false
    },
    status: {
        type: Number,
        required:true,
        default: 1
    }

});

module.exports = categorySchema;