const mongoose = require('mongoose');
const baseModel = require('./BaseModel');
const Schema = mongoose.Schema;

//字段介绍
/**
 * announcementid:公告id
 * announcement:公告
 */

const announcementSchema = new mongoose.Schema({
    ...baseModel,
    announcement: {
        type: String,
        required: true,
    }
});

module.exports = announcementSchema;