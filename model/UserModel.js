const mongoose = require('mongoose');
const baseModel = require('./BaseModel');
const md5 = require('../util/md5');

/**
 * 字段介绍
 * username:用户名
 * password:密码
 * phone:电话
 * role:权限（1表示管理员，0为普通用户）
 * createdAt:创建时间
 * updatedAt
 */

const userSchema = new mongoose.Schema({
    ...baseModel,
    username: {
        type: String,
        require: true
    },
    password: {
        type: String,
        required: true,
        set: value => md5(value),//加密存储
        select:false        //默认查询的时候不会带出
    },
    phone: {
        type: String,
        require: true
    },
    role:{
        type:Number,
        require:true,
        default:0
    }
});

module.exports = userSchema;