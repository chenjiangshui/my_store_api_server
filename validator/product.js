const { body } = require('express-validator');
const validate = require('../middleware/validate');
const { ProductModel } = require('../db');

//添加分类 校验
exports.addProduct = validate([
    body('name')
        .notEmpty().withMessage('商品名称不能为空')
        .isLength({ max: 16 }).withMessage('图片名称大于16')
        .bail()
        .custom(async name => {
            const carousel = await ProductModel.findOne({ name });
            if (carousel) {
                return Promise.reject('商品名称已存在');
            }
        }),
    body('mainImageUrl')
        .notEmpty().withMessage('主图片地址不能为空'),
    body('imgUrl1')
        .notEmpty().withMessage('图片1地址不能为空'),
    body('imgUrl2')
        .notEmpty().withMessage('图片2地址不能为空'),
    body('imgUrl3')
        .notEmpty().withMessage('图片3地址不能为空'),
    body('imgUrl4')
        .notEmpty().withMessage('图片4地址不能为空'),
    body('imgUrl5')
        .notEmpty().withMessage('图片5地址不能为空'),
    body('price')
        .notEmpty().withMessage('商品价格不能为空'),
    body('status')
        .notEmpty().withMessage('商品状态不能为空'),
]);