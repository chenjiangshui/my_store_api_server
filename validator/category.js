const { body } = require('express-validator');
const validate = require('../middleware/validate');
const { CategoryModel } = require('../db');

//添加分类 校验
exports.addCategory = validate([
    body('category_name')
        .notEmpty().withMessage('分类名称不能为空')
        .isLength({ max: 16 }).withMessage('分类名称大于16')
        .bail()
        .custom(async category_name => {
            const category = await CategoryModel.findOne({ category_name });
            if (category) {
                return Promise.reject('分类已存在');
            }
        })
]);

//更新分类 校验
exports.updateCategory = validate([
    body('category_name')
        .isLength({ max: 16 }).withMessage('分类名称大于16')
        .bail()
        .custom(async category_name => {
            const category = await CategoryModel.findOne({ category_name });
            if (category) {
                return Promise.reject('分类已存在');
            }
        })
]);