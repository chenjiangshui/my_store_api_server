const { body } = require('express-validator');
const validate = require('../middleware/validate');
const { UserModel } = require('../db');
const md5 = require('../util/md5');

//用户注册 校验
exports.register = validate([
    //非空校验
    body('username')
        .notEmpty().withMessage('用户名不能为空')
        .custom(async username => {    //自定义校验规则
            const user = await UserModel.findOne({ username });
            if (user) {
                return Promise.reject('用户已存在');
            }

        }),

    body('password').notEmpty().withMessage('密码不能为空').isLength({ min: 6 }).withMessage('密码长度小于6').isLength({ max: 16 }).withMessage('密码长度大于16'),

    body('phone').
        notEmpty().withMessage('手机号不能为空')//非空校验
        .isMobilePhone().withMessage('手机号格式不正确')//校验是否是手机号
        .custom(async phone => {
            const user = await UserModel.findOne({ phone });
            if (user) {
                return Promise.reject('手机号已存在');
            }
        })
]);

//用户登录 校验
exports.login = [
    validate([
        body('username').custom(async (username, { req }) => {
            //查询后续需要的数据 select是后续需要的数据
            const user = await UserModel.findOne({ username })
                .select(['username', 'password', 'phone', 'role']);
            if (!user) {
                return Promise.reject('用户不存在');
            }
            //将数据挂载到请求对象中，后续中间件也可以使用了
            req.user = user;
        })
    ]),
    validate([
        body('password').custom(async (password, { req }) => {
            if (md5(password) !== req.user.password) {
                return Promise.reject('密码错误');
            }
        })
    ]),
];