const { body } = require('express-validator');
const validate = require('../middleware/validate');
const { CarouselModel } = require('../db');

//添加分类 校验
exports.addCarousel = validate([
    body('imgName')
        .notEmpty().withMessage('图片名称不能为空')
        .isLength({ max: 16 }).withMessage('图片名称大于16')
        .bail()
        .custom(async imgName => {
            const carousel = await CarouselModel.findOne({ imgName });
            if (carousel) {
                return Promise.reject('图片名称已存在');
            }
        }),
    body('imgUrl')
        .notEmpty().withMessage('图片地址不能为空')
]);