const jwt = require('jsonwebtoken');

const {promisify} =  require('util')

//异步生成jwt
exports.sign = promisify(jwt.sign)

//异步校验jwt
exports.verify = promisify(jwt.verify)

//不验证，直接解析jwt
exports.decode = promisify(jwt.decode)





//生成jwt
// const token = jwt.sign({
//     foo: 'bar'
// }, 'chenjiangshui');
// console.log(token);

// //异步生成jwt
// jwt.sign({
//     foo: 'bar'
// }, 'chenjiangshui', (err, token) => {
//     if (err) {
//         return console.log('生成token失败');
//     }
//     console.log(token);
// });

//验证jwt
// const ret = jwt.verify('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIiLCJpYXQiOjE3MDIxNzk3MTZ9.4C5Qvpd0YvfInUOP229ODiSfPxGLS8FmybXN4bd28vY','chenjiangshui')
// console.log(ret);

//异步验证jwt
// jwt.verify('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIiLCJpYXQiOjE3MDIxNzk3MTZ9.4C5Qvpd0YvfInUOP229ODiSfPxGLS8FmybXN4bd28vY', 'chenjiangshui', (err, ret) => {
//     if (err) {
//         return console.log('token 认证失败');
//     }
//     console.log(ret);
// });
