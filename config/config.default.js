/**
 * 默认配置
 */

module.exports = {
    dbUri: "mongodb://127.0.0.1:27017/store",   //数据库地址
    jwtSecret:'564db13f-3036-8fde-f5cb-c974b4e5380c'    //生成jwt唯一的值，后续不要更改
};