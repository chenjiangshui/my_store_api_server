const mongoose = require('mongoose');
const { dbUri } = require('../config/config.default');

mongoose.connect(dbUri);

const db = mongoose.connection;

//当连接失败的时候
db.on('error', err => {
    console.log('MongoDB 数据库连接失败', err);
});

//当连接成功的时候
db.once('open', function () {
    console.log('数据库连接成功');
});

//组织导出模型类 
module.exports = {
    UserModel: mongoose.model('User', require('../model/UserModel')),
    CategoryModel: mongoose.model('Category', require('../model/CategoryModel')),
    CarouselModel: mongoose.model('carousel', require('../model/CarouselModel')),
    ProductModel: mongoose.model('product', require('../model/ProductModel')),
    CartModel: mongoose.model('Cart', require('../model/CartModel')), 
    AnnouncementModel: mongoose.model('announcement', require('../model/AnnouncementModel')), 
};