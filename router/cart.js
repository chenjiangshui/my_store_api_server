const express = require('express');

const router = express.Router();

//引入执行函数
const cartCtrl = require('../controller/cart');

//引入验证中间件
// const cartValidator = require('../validator/cart');

//获取购物车列表
router.get('/cartList',cartCtrl.getCartList)

//添加购物车
router.post('/cart',cartCtrl.addCart);

//结算购物车
router.post('/updateCart',cartCtrl.UpdateCart);

//获取订单列表
router.get('/OrderList',cartCtrl.getOrderList)


module.exports = router;