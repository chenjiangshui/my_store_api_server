const express = require('express');

const router = express.Router();

//引入执行函数
const categoryCtrl = require('../controller/category');

//引入验证中间件
const categoryValidator = require('../validator/category');

//获取分类列表
router.get('/categoryList',categoryCtrl.getCategoryList)

//添加分类
router.post('/category',categoryValidator.addCategory,categoryCtrl.addCategory);

//更新分类
router.patch('/category',categoryValidator.updateCategory,categoryCtrl.updateCategory)

//删除分类
router.delete('/category/:categoryid',categoryCtrl.deleteCategory)


module.exports = router;