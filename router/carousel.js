const express = require('express');

const router = express.Router();

//引入执行函数
const carouselCtrl = require('../controller/carousel');

//引入验证中间件
const carouselValidator = require('../validator/carousel');

//获取轮播图列表
router.get('/carouselList',carouselCtrl.getCarouselList)

//添加轮播图
router.post('/carousel',carouselValidator.addCarousel,carouselCtrl.addCarousel);


module.exports = router;