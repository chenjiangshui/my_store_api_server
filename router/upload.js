const express = require('express');

const router = express.Router();
//引入执行函数
const uploadCtrl = require('../controller/upload.js');


//上传产品图片
router.post('/upload/productImage',uploadCtrl.uploadProductImage)


module.exports = router;