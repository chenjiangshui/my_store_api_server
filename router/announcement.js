const express = require('express');

const router = express.Router();

//引入执行函数
const announcementCtrl = require('../controller/announcement');

//获取公告列表
router.get('/announcementList',announcementCtrl.getAnnouncementList)

//添加公告
router.post('/announcement',announcementCtrl.addAnnouncement);

module.exports = router;