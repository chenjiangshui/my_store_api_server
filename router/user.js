const express = require('express');

const router = express.Router();

//引入执行函数
const userCtrl = require('../controller/user');

//引入验证中间件
const userValidator = require('../validator/user');
//引入获取当前登录用户
const user = require('../middleware/user');


//注册用户
router.post('/user', userValidator.register, userCtrl.register);

//登录用户
router.post('/user/login',userValidator.login,userCtrl.login)

//获取用户列表
router.get('/userList',userCtrl.getUserList)

//删除用户
router.delete('/user/:id',userCtrl.deleteUser);

// //获取当前登录用户
// router.get('/user', user, userCtrl.getCurrentUser);


module.exports = router;