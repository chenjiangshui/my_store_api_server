const express = require('express');
const router = express.Router();
//引入统一响应数据格式中间件
const resextra = require('../middleware/resextra')

//统一响应数据格式中间件
router.use(resextra)

//用户相关路由
router.use(require('./user'));

//商品类别相关路由
router.use(require('./category'))

//轮播图相关路由
router.use(require('./carousel'))

//商品相关路由
router.use(require('./product'))

//购物车相关路由
router.use(require('./cart'))

//上传路由
router.use(require('./upload'))

//公告相关路由
router.use(require('./announcement'))

module.exports = router;