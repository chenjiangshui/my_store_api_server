const express = require('express');

const router = express.Router();

//引入执行函数
const productCtrl = require('../controller/product');

//引入验证中间件
const productValidator = require('../validator/product');

//获取商品列表
router.get('/productList',productCtrl.getProductList)

//获取商品详情
router.get('/product/:id',productCtrl.getProductDetail)

//添加商品
router.post('/product',productValidator.addProduct,productCtrl.addProduct);

//更新商品
router.patch('/product',productCtrl.updateProduct)

//删除商品
router.delete('/product/:id',productCtrl.deleteProduct);


module.exports = router;