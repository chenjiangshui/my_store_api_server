const { validationResult, buildCheckFunction } = require('express-validator');

const { isValidObjectId } = require('mongoose');

//exports = module.exports 由于已经作为函数暴露，这样可以重新指向，在下面就可以接着暴露,在后续使用只需要.出后面暴露的函数就可以
exports = module.exports = validations => {
  return async (req, res, next) => {

    //非链式调用，同时校验
    await Promise.all(validations.map(validation => validation.run(req)));
    //链式调用，不会同时校验
    // for (let validation of validations) {
    //   const result = await validation.run(req);
    //   if (result.errors.length) break;
    // }

    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }

    res.status(400).json({ errors: errors.array() });
  };
};

//封装是否是mongdb的id
exports.isValidObjectId = (location, fields) => {
  return buildCheckFunction(location)(fields).custom(async value => {
    if (!isValidObjectId(value)) {
      return Promise.reject('ID 不是一个有效的ObjectID');
    }
  });
};