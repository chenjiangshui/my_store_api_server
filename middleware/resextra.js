//响应请求统一格式
module.exports = (req, res, next) => {
    //定义一个函数，传参数，固定返回值
    res.sendResult2 = (data, code, message) => {
        // console.log(req.query);
        res.json({
            "data": data,
            "meta": {
                "msg": message,
                "status": code
            }
        });
    };
    next();
};