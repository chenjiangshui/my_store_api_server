const util = require('util');

module.exports = () => {
    return (err, req, res, next) => {
        res.status(500).json({ 
            //使用util.format(),将错误信息转为格式支持,仅用于开发
            error: util.format(err) 
        });
    };
};