const express = require('express');
const path = require('path')
const morgan = require('morgan');
const cors = require('cors');
const router = require('./router')
const errorHandler = require('./middleware/error-handler')


//使用数据库
require('./db/index')

const app = express();

//日志中间件
app.use(morgan('dev'))

//解析json格式中间件
app.use(express.json());

//跨域资源请求中间件
app.use(cors());


//提供兼容性,解析urlencoded
app.use(express.urlencoded())

//静态资源托管
app.use('/public',express.static(path.join(__dirname,'./public')))


//优先通过环境变量设置端口号,如果环境变量没有设置,默认为9999
const PORT = process.env.PORT || 9999;

//挂载路由
app.use('/api/private/v1/',router)

//挂载统一处理服务端错误中间件
app.use(errorHandler())

app.listen(PORT, () => {
    console.log(`Server is running at http://localhost:${PORT}`);
});