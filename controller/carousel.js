//引入数据库文件
const { CarouselModel } = require('../db');

//获取轮播图列表
exports.getCarouselList = async (req, res, next) => {
    try {
        let carouselList = await CarouselModel.find();
        res.status(200).sendResult2(carouselList, 200, '获取成功');
    } catch (error) {
        next(error);
    }
};

//添加轮播图
exports.addCarousel = async (req, res, next) => {
    try {

        let carousel = await CarouselModel.create(req.body);

        res.status(201).sendResult2(carousel, 201, '添加成功');

    } catch (error) {
        next(error);

    }
};