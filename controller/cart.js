//引入数据库文件
const { CartModel } = require('../db');

//获取购物车列表
exports.getCartList = async (req, res, next) => {
    try {

        //定义用户传递过来的参数 并设置默认值
        const { limit = 5, page = 1, userid } = req.query;
        offset = (page - 1) * limit;

        //获取总条数
        let cartCount = await CartModel.countDocuments({ userid: userid, status: 0 });
        let cartList = await CartModel.find({ userid: userid, status: 0 })
            .skip(Number.parseInt(offset)) //跳过多少条
            .limit(Number.parseInt(limit)) //取多少条
            .sort({
                //-1倒序 1升序
                createdAt: 1
            });
        res.status(200).sendResult2({ cartList, cartCount }, 200, '获取成功');

    } catch (error) {
        next(error);
    }
};

//添加购物车
exports.addCart = async (req, res, next) => {
    try {
        let cart = await CartModel.findOne({ userid: req.body.userid, proid: req.body.proid, status: 0 });
        if (cart) {
            let n_quantity = Number(cart.quantity) + Number(req.body.quantity);
            console.log('找到了',n_quantity);
            let newCart = await CartModel.updateOne({ userid: req.body.userid, proid: req.body.proid,status: 0 }, { $set: { quantity: n_quantity } });
            
            res.status(201).sendResult2(newCart, 201, '添加成功');


        } else {
            console.log('没找到');
            let newCart = await CartModel.create(req.body);
            console.log(newCart);
            res.status(201).sendResult2(newCart, 201, '添加成功');
        }

    } catch (error) {
        next(error);

    }
};

//结算购物车
exports.UpdateCart = async (req, res, next) => {
    try {
        req.body.multipleSelection.forEach(async item => {
            await CartModel.updateOne({ _id: item._id }, { $set: { status: 1 } });
        });
        res.status(200).sendResult2({}, 200, '结算成功');

    } catch (error) {
        next(error);
    }
};

//获取订单列表
exports.getOrderList = async (req, res, next) => {
    try {

        //定义用户传递过来的参数 并设置默认值
        const { limit = 5, page = 1, userid } = req.query;
        offset = (page - 1) * limit;

        //获取总条数
        let cartCount = await CartModel.countDocuments({ userid: userid, status: 1 });
        let cartList = await CartModel.find({ userid: userid, status: 1 })
            .skip(Number.parseInt(offset)) //跳过多少条
            .limit(Number.parseInt(limit)) //取多少条
            .sort({
                //-1倒序 1升序
                createdAt: 1
            });
        res.status(200).sendResult2({ cartList, cartCount }, 200, '获取成功');

    } catch (error) {
        next(error);
    }
};