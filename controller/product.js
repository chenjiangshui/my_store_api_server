//引入数据库文件
const { ProductModel, CategoryModel } = require('../db');
//导入验证mongodbid的中间件
const validate = require('../middleware/validate');

//获取商品列表
exports.getProductList = async (req, res, next) => {
    try {

        //定义用户传递过来的参数 并设置默认值
        const { limit = 5, page = 1, cateid, proName } = req.query;
        offset = (page - 1) * limit;

        if (cateid == '') {

            let name = new RegExp(proName, 'i');
            //获取总条数
            let productsCount = await ProductModel.countDocuments({ name });

            let productList = await ProductModel.find({ name })
                .skip(Number.parseInt(offset)) //跳过多少条
                .limit(Number.parseInt(limit)) //取多少条
                .sort({
                    //-1倒序 1升序
                    createdAt: 1
                });
            res.status(200).sendResult2({ productList, productsCount }, 200, '获取成功');
        } else {
            //获取总条数
            let productsCount = await ProductModel.countDocuments({ cateid: cateid });
            let productList = await ProductModel.find({ cateid: cateid })
                .skip(Number.parseInt(offset)) //跳过多少条
                .limit(Number.parseInt(limit)) //取多少条
                .sort({
                    //-1倒序 1升序
                    createdAt: 1
                });
            res.status(200).sendResult2({ productList, productsCount }, 200, '获取成功');
        }



    } catch (error) {
        next(error);
    }
};

//获取商品详情
exports.getProductDetail = async (req, res, next) => {
    try {

        console.log(req.params);
        const product = await ProductModel.findById(req.params.id).populate('cateid');
        // console.log(product);
        const category = await CategoryModel.findById(product.cateid._id);
        // console.log(category);

        if (!product) {
            return res.status(404).end();
        }
        res.status(200).sendResult2({ product, category }, 200, '获取成功');
    } catch (error) {
        next(error);
    }
};



//添加商品列表
exports.addProduct = async (req, res, next) => {
    try {

        let product = await ProductModel.create(req.body);

        res.status(201).sendResult2(product, 201, '添加成功');

    } catch (error) {
        next(error);

    }
};


//更新商品
exports.updateProduct = async (req, res, next) => {
    try {

        //验证Productid
        validate.isValidObjectId(['req.body'], 'productid');

        //获取到需要修改的参数
        let { name,productid,price } = req.body;

        //修改参数
        if (name) {
            await ProductModel.updateOne({ _id: productid }, { $set: { name: name, updatedAt: new Date() } });
            await ProductModel.updateOne({ _id: productid }, { $set: { price: price, updatedAt: new Date() } });

        }


        let product = await ProductModel.findById(productid);

        res.status(201).sendResult2(product, 201, '添加成功');
    } catch (error) {
        next(error);
    }
};


//删除产品
exports.deleteProduct = async (req, res, next) => {
    try {
        //验证id
        validate.isValidObjectId(['req.params'], 'id');

        let { id } = req.params;

        //获取商品
        let product = await ProductModel.findById(id);

        if (product) {
            //删除商品
            await ProductModel.deleteOne({ _id: id });
            res.status(204).sendResult2(null, 204, '删除成功');
        } else {
            res.status(500).sendResult2(null, 500, '删除失败，没有找到商品');
        }

    } catch (error) {
        next(error);
    }
};