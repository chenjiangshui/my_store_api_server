//引入数据库模板
const { UserModel } = require('../db');
const jwt = require('../util/jwt');
const { jwtSecret } = require('../config/config.default');
//导入验证mongodbid的中间件
const validate = require('../middleware/validate');

//用户登录
exports.login = async (req, res, next) => {
    try {
        //1.数据验证
        //2.验证成功,将数据保存到数据库
        //3.发送成功响应
        //4.处理请求

        //req.user在validate中间件当中已经配置了user,转为普通对象
        const user = req.user.toJSON();
        const token = await jwt.sign(
            { userId: user._id },
            jwtSecret,
            {
                expiresIn: 5   //过期时间：30s
                // expiresIn: 60 * 60 * 24 //一天
            }
        );

        //发送响应 移除password
        delete user.password;

        res.status(200).sendResult2({ user, token }, 200, '登录成功');
    } catch (err) {
        next(err);
    }
};


//用户注册
exports.register = async (req, res, next) => {
    try {
        let user = await UserModel.create(req.body);
        res.status(201).sendResult2(user, 201, '注册成功');
    } catch (error) {
        next(error);
    }
};


//获取用户列表
exports.getUserList = async (req, res, next) => {
    try {

        //定义用户传递过来的参数 并设置默认值
        const { limit = 5, page = 1, usname } = req.query;
        offset = (page - 1) * limit;


        console.log(usname);
        let username = new RegExp(usname, 'i');
        //获取总条数
        let usersCount = await UserModel.countDocuments({ username,role:0 });

        let userList = await UserModel.find({ username,role:0  })
            .skip(Number.parseInt(offset)) //跳过多少条
            .limit(Number.parseInt(limit)) //取多少条
            .sort({
                //-1倒序 1升序
                createdAt: 1
            });
        res.status(200).sendResult2({ userList, usersCount }, 200, '获取成功');


    } catch (error) {
        next(error);
    }
};

//删除用户
exports.deleteUser = async (req, res, next) => {
    try {
        //验证id
        validate.isValidObjectId(['req.params'], 'id');

        let { id } = req.params;

        //获取商品
        let user = await UserModel.findById(id);

        if (user) {
            //删除用户
            await UserModel.deleteOne({ _id: id });
            res.status(204).sendResult2(null, 204, '删除成功');
        } else {
            res.status(500).sendResult2(null, 500, '删除失败，没有找到用户');
        }

    } catch (error) {
        next(error);
    }
};


//获取当前登录用户
// exports.getCurrentUser = async (req, res, next) => {
//     try {
//         res.status(200).sendResult2(req.user, 200, '获取成功');
//     } catch (err) {
//         next(err);
//     }
// };