//引入数据库文件
const { CategoryModel } = require('../db');
//导入验证mongodbid的中间件
const validate = require('../middleware/validate');

//添加分类
exports.addCategory = async (req, res, next) => {
    try {

        //由于在validate中封装了校验是否为mongodbid的方法
        //如果存在父分类
        if (req.body.parentid) {
            validate.isValidObjectId(['req.body'], 'parentid');
            //将父亲没有孩子的状态改为有
            await CategoryModel.updateOne({ _id: req.body.parentid }, { $set: { isChild: true, updatedAt: new Date() } });

            let category = await CategoryModel.create(req.body);
            res.status(201).sendResult2(category, 201, '添加成功');
        } else {
            req.body.isChild = true;
            let category = await CategoryModel.create(req.body);
            res.status(201).sendResult2(category, 201, '添加成功');

        }

    } catch (error) {
        next(error);
    }
};

//获取分类列表
exports.getCategoryList = async (req, res, next) => {
    try {
        //定义用户传递过来的参数 并设置默认值
        const { limit = 5, page = 1 } = req.query;
        offset = (page - 1) * limit;

        //获取总条数
        let cateCount = await CategoryModel.countDocuments({ status: 1 });
        let cateList = await CategoryModel.find({ status: 1 })
            .skip(Number.parseInt(offset)) //跳过多少条
            .limit(Number.parseInt(limit)) //取多少条
            .sort({
                //-1倒序 1升序
                createdAt: 1
            });

        let cateAllList = await CategoryModel.find({ status: 1 });

        //获取一级菜单
        let cateOneList = []
        cateAllList.forEach(async item => {
            
            if (item.isChild == true) {
                cateOneList.push(item);
            }
        });
        
        res.status(200).sendResult2({ cateList,cateOneList,cateCount }, 200, '获取成功');

    } catch (error) {
        next(error);
    }
};

//更新分类
exports.updateCategory = async (req, res, next) => {
    try {

        //验证categoryid
        validate.isValidObjectId(['req.body'], 'categoryid');

        //获取到需要修改的参数
        let { category_name,categoryid } = req.body;

        //修改参数
        if (category_name) {
            await CategoryModel.updateOne({ _id: categoryid }, { $set: { category_name: category_name, updatedAt: new Date() } });

        }
        // if (status == 1 || status == 0) {
        //     await CategoryModel.updateOne({ _id: categoryid }, { $set: { status: status, updatedAt: new Date() } });
        // }
        // if (parentid) {

        //     //获取之前的父亲id
        //     let categorybefore = await CategoryModel.findById(categoryid);
        //     let categoryParentid = categorybefore.parentid;

        //     //改变现在的父亲状态 start
        //     await CategoryModel.updateOne({ _id: categoryid }, { $set: { parentid: parentid, updatedAt: new Date() } });
        //     //由于在validate中封装了校验是否为mongodbid的方法
        //     //如果存在父分类
        //     validate.isValidObjectId(['req.body'], 'parentid');
        //     //将父亲没有孩子的状态改为有
        //     await CategoryModel.updateOne({ _id: req.body.parentid }, { $set: { isChild: true, updatedAt: new Date() } });
        //     //改变现在的父亲状态 end

        //     //当所有的parentid都不等于传过来的parentid时，这个状态变为false
        //     //获取到当前的项，拿到父亲id,再去找到父亲id，更改状态
        //     //记录状态
        //     let categoryIschild = false;

        //     let categoryAll = await CategoryModel.find();
        //     categoryAll.map(category => {
        //         if (category.parentid != null) {
        //             if (categoryParentid.toJSON() == category.parentid.toJSON()) {
        //                 categoryIschild = true;
        //             }
        //         }

        //     });
        //     //改变之前的父亲isChild
        //     await CategoryModel.updateOne({ _id: categoryParentid }, { $set: { isChild: categoryIschild, updatedAt: new Date() } });

        // }


        let category = await CategoryModel.findById(categoryid);

        res.status(201).sendResult2(category, 201, '添加成功');
    } catch (error) {
        next(error);
    }
};

//删除分类
exports.deleteCategory = async (req, res, next) => {
    try {
        //验证categoryid
        validate.isValidObjectId(['req.params'], 'categoryid');

        let { categoryid } = req.params;



        //获取之前的父亲id
        let categorybefore = await CategoryModel.findById(categoryid);
        let categoryParentid = categorybefore.parentid;

        if (categoryParentid) {
            //删除分类
            await CategoryModel.deleteOne({ _id: categoryid });

            //当所有的parentid都不等于传过来的parentid时，这个状态变为false
            //获取到当前的项，拿到父亲id,再去找到父亲id，更改状态
            //记录状态
            let categoryIschild = false;

            let categoryAll = await CategoryModel.find();
            categoryAll.map(category => {
                if (category.parentid != null) {
                    if (categoryParentid.toJSON() == category.parentid.toJSON()) {
                        categoryIschild = true;
                    }
                }

            });
            //改变之前的父亲isChild
            await CategoryModel.updateOne({ _id: categoryParentid }, { $set: { isChild: categoryIschild, updatedAt: new Date() } });

            res.status(204).sendResult2(null, 204, '删除成功');
        } else {
            //删除分类
            await CategoryModel.deleteOne({ _id: categoryid });

            await CategoryModel.deleteMany({ parentid: categoryid });
            res.status(204).sendResult2(null, 204, '删除成功');
        }





    } catch (error) {
        next(error);
    }
};