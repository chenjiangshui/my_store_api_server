const multiparty = require("multiparty");
//导入fs模块
const fs = require('fs');

//上传产品图片
exports.uploadProductImage = async (req, res, next) => {
    try {
        let form = new multiparty.Form();

        form.uploadDir = "./public/productImages";

        form.keepExtensions = true;   //是否保留后缀
        form.parse(req, function (err, fields, files) {  //其中fields表示你提交的表单数据对象，files表示你提交的文件对象
            // console.log(req);
            // console.log(fields, files);
            //重命名后的路径名称
            imageSrc = './public/productImages/' + files.file[0].originalFilename
            //重写文件
            fs.rename(files.file[0].path, imageSrc, err => {
                if (err) {
                    console.log('操作失败~');
                    return;
                }
                console.log('操作成功');
            });

            if (err) {
                res.json({
                    status: "1",
                    msg: "上传失败！" + err
                });
            } else {
                console.log(files);
                res.json({
                    status: "0",
                    msg: "上传成功！",
                    imgSrc: files.file[0].originalFilename
                });
            }
        });
    } catch (error) {
        next(error);
    }
};
