//引入数据库文件
const { AnnouncementModel } = require('../db');

//获取公告，只获取最后5条
exports.getAnnouncementList = async (req, res, next) => {
    try {
        //定义用户传递过来的参数 并设置默认值
        const { limit = 5, page = 1 } = req.query;
        offset = (page - 1) * limit;

        //获取总条数
        let announcementsCount = await AnnouncementModel.countDocuments();

        let announcementList = await AnnouncementModel.find()
            .skip(Number.parseInt(offset)) //跳过多少条
            .limit(Number.parseInt(limit)) //取多少条
            .sort({
                //-1倒序 1升序
                createdAt: -1
            });
        res.status(200).sendResult2({ announcementList, announcementsCount }, 200, '获取成功');
    } catch (error) {
        next(error);
    }
};

//添加公告
exports.addAnnouncement = async (req, res, next) => {
    try {

        let announcement = await AnnouncementModel.create(req.body);

        res.status(201).sendResult2(announcement, 201, '添加成功');

    } catch (error) {
        next(error);

    }
};